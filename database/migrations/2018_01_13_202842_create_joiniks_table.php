<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJoiniksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joiniks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('description')->nullable();
            /*
             * factor that describes the availability of the meeting for other users.
             * it will contains the following options: private, public, friendly
             *
             * public - 0 - вступить может любой пользователь, видна везде
             *
             * friendly - 1 - вступить может только друг создателя встречи
             * видна только у друзей
             *
             * private - 2 - вступить другие пользователи могут только по прямому приглашению от создателя встречи.
             * недоступны в поиске, в списке у друзей и где-либо еще.
             *
             */
            $table->tinyInteger('type_available')->default(0);
            /*
             * если protected в '1' - встреча закрыта, доступно название, описание, город и дата, но полный доступ предоставляется
             * только после подтверждения участия создателем встречи.
             */
            $table->boolean('protected')->default(0);

            /*
             * if the meeting is gaining the maximum number of participants, then it becomes more inaccessible while searching,
             * but remains accessible to friends without the possibility of joining
             */
            $table->integer('is_limited_members')->default(0);
            $table->integer('max_count_members')->nullable();

            /*
             * Ограничение по возрасту
             */
            $table->boolean('limit_age_from')->default(0);
            $table->boolean('limit_age_to')->default(0);

            $table->string('thumbnail')->nullable();


            $table->boolean('deleted')->default(0);

            $table->timestamp('date_start')->useCurrent();
            $table->timestamp('date_end')->useCurrent();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joiniks');
    }
}
