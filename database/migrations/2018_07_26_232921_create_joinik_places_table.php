<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJoinikPlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joinik_places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('joinik_id');
            $table->string('street');
            $table->string('additional_info')->nullable();
            $table->float('coords_x')->nullable();
            $table->float('coords_y')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joinik_places');
    }
}
