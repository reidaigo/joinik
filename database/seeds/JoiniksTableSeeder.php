<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class JoiniksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,6) as $index) {
            DB::table('joiniks')->insert([
                'title' => $faker->word,
                'description' => $faker->paragraph,
                'thumbnail' => $faker->word,
                'date_start' => date("Y-m-d H:i:s",time() - 10000),
                'date_end' => date("Y-m-d H:i:s",time() + 10000),
            ]);
        }
    }
}
