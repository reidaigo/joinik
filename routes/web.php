<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/

// Page Routes
Route::get('/', 'PageController@main')->name('main');

// Joinik Routes
//Route::get('joinik/create', 'Frontend\JoinikController@create');

Route::middleware(['auth'])->group(function () {
    Route::resources([
        'joinik' => 'JoinikController',
    ]);

//    Route::post('/', 'JoinikController@store')->name('main');

});

