ymaps.ready(init);

function init() {

    var startCoords = [];

    ymaps.geolocation.get({
        // Выставляем опцию для определения положения по ip
        provider: 'browser',
        // Карта автоматически отцентрируется по положению пользователя.
        mapStateAutoApply: true
    }).then(function (result) {

        console.log(result.geoObjects.position);
        startCoords = result.geoObjects.position;

        var myMap = new ymaps.Map("map", {
                center: startCoords,
                zoom: 10
            }, {
                searchControlProvider: 'yandex#search'
            }),

            // Создаем геообъект с типом геометрии "Точка".
            myGeoObject = new ymaps.GeoObject({
                // Описание геометрии.
                geometry: {
                    type: "Point",
                    coordinates: startCoords
                },
                // Свойства.
                properties: {
                    // Контент метки.
                    iconContent: 'Я тащусь',
                    hintContent: 'Ну давай уже тащи'
                }
            }, {
                // Опции.
                // Иконка метки будет растягиваться под размер ее содержимого.
                preset: 'islands#blackStretchyIcon',
                // Метку можно перемещать.
                draggable: true
            });




        myMap.geoObjects
            .add(myGeoObject);

        myGeoObject.events.add("dragend", function (event) {
            console.log(myGeoObject.geometry.getCoordinates());
        });

        myMap.events.add("click", function (event) {
            console.log(myGeoObject.geometry.getCoordinates());
        });

        // Поставим метку по клику над картой
        myMap.events.add('click', function (e) {
            // Географические координаты точки клика можно узнать
            // посредством вызова .get('coordPosition')
            var coords = e.get('coords');
            myGeoObject.geometry.setCoordinates(coords);
        });
    });


}
