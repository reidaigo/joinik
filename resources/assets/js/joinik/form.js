JoinikForm = {

    init: function(){

        this.handleAdditionalOptionsClick();
        this.selectDatesInit();

    },

    //разворачиваем дополнительные настройки в форме создания встречи
    handleAdditionalOptionsClick: function (pageType) {

        $(document).on('click', '#additional_options_form', function() {
            $(this).find('i.arrow-badge').toggleClass('glyphicon-arrow-down').toggleClass('glyphicon-arrow-up');

            $('.additional_options').toggleClass('hidden');
        });
    },

    selectDatesInit: function () {
        $(function () {
            $('#datetimepicker_start').datetimepicker({
                locale: 'ru'
            });
            $('#datetimepicker_end').datetimepicker({
                locale: 'ru'
            });
        });
    }

};