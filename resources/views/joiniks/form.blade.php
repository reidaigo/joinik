@extends('layouts.app')

@section('title', __('joinik.titles.create') . ' - ')

@section('content')
    <form action="/joinik" method="post" enctype="multipart/form-data">

        {{ csrf_field() }}
        
        <div class="form-group col-md-12">
            <label for="name" class="control-label">
                <i class="fa fa-info-circle" data-toggle="popover"></i>
                {{ trans('joinik.form.name') }} <span class="required_label">*</span>
            </label>
            <input type="text" class="form-control" name="title" id="name"
                   placeholder="{{ trans('joinik.form.placeholder_name') }}"
                   value="{{ old('name', $joinik->name) }}"
                   required
            >
            <span class="help-block"></span>
        </div>

        <div class="form-group col-md-12">
            <label for="description" class="control-label">
                <i class="fa fa-info-circle" data-toggle="popover"></i>
                {{ trans('joinik.form.description') }}
            </label>
            <textarea type="text" class="form-control" name="description" id="description"
                      placeholder="{{ trans('joinik.form.placeholder_description') }}"
            >
                {{ old('description', $joinik->description) }}
            </textarea>
            <span class="help-block"></span>
        </div>

        <div class="form-group col-md-6">
            <label for="place_str" class="control-label">
                <i class="fa fa-info-circle" data-toggle="popover"></i>
                {{ trans('joinik.form.place_str.label') }}
            </label>
            <input type="text" class="form-control" name="street" id="place_str"
                   placeholder="{{ trans('joinik.form.place_str.placeholder') }}"
                   value="{{ old('street', $joinik->street) }}"
                   required
            >
            <span class="help-block"></span>
        </div>

        <div class="form-group col-md-6">
            <label for="place_add_info" class="control-label">
                <i class="fa fa-info-circle" data-toggle="popover"></i>
                {{ trans('joinik.form.place_add_info.label') }}
            </label>
            <input type="text" class="form-control" name="additional_info" id="place_add_info"
                   placeholder="{{ trans('joinik.form.place_add_info.placeholder') }}"
                   value="{{ old('additional_info', $joinik->place_add_info) }}"
            >
            <span class="help-block"></span>
        </div>


        <div class="form-group col-md-6">
            <label for="description" class="control-label">
                <i class="fa fa-info-circle" data-toggle="popover"></i>
                {{ trans('joinik.form.date_start') }} <span class="required_label">*</span>
            </label>
            <div class='input-group date'>
                <input type="text" class="form-control" name="description" id="datetimepicker_start"
                       placeholder="{{ trans('joinik.form.placeholder_date_start') }}"
                       value="{{ old('date_start', $joinik->date_start) }}"
                       required
                >
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>

        <div class="form-group col-md-6">
            <label for="description" class="control-label">
                <i class="fa fa-info-circle" data-toggle="popover"></i>
                {{ trans('joinik.form.date_end') }}
            </label>
            <div class='input-group date'>
                <input type="text" class="form-control" name="description" id="datetimepicker_end"
                       placeholder="{{ trans('joinik.form.placeholder_date_end') }}"
                       value="{{ old('date_end', $joinik->date_end) }}"
                >
                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>

        <div class="form-group col-md-12">
            <a class="dropdown-toggle" href="#" id="additional_options_form">{{ trans('joinik.form.additional_options') }}<i class="glyphicon glyphicon-arrow-down arrow-badge"></i></a>
        </div>

        <div class="additional_options">
            <div class="form-group col-md-12">
                <label for="type_available" class="control-label">
                    <i class="fa fa-info-circle" data-toggle="popover"></i>
                    {{ trans('joinik.form.type_available.label') }}
                </label>

                <div class="radio">
                    <label for="ta_public">
                        <input name="type_available" type="radio" id="ta_public" value="0" checked>
                        {{ trans('joinik.form.type_available.opts.public') }}
                    </label>
                    <span class="glyphicon glyphicon-info"></span>
                </div>

                <div class="radio">
                    <label for="ta_friendly">
                        <input name="type_available" type="radio" id="ta_friendly" value="1">
                        {{ trans('joinik.form.type_available.opts.friendly') }}
                    </label>
                </div>

                <div class="radio">
                    <label for="ta_private">
                        <input name="type_available" type="radio" id="ta_private" value="2">
                        {{ trans('joinik.form.type_available.opts.private') }}
                    </label>
                </div>
            </div>

            <div class="form-group col-md-12">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="protected" value="1">{{ trans('joinik.form.protected') }}
                    </label>
                </div>
            </div>

            <div class="form-group col-md-12">
                <label for="name" class="control-label">
                    <i class="fa fa-info-circle" data-toggle="popover"></i>
                    {{ trans('joinik.form.max_count_members.label') }}
                </label>

                <div class="input-group">
                <span class="input-group-addon">
                    <input name="is_limited_members" class="limit_members" type="radio" id="no_limit_members"
                           value="0"
                           checked>
                </span>
                    <label for="no_limit_members">
                        &nbsp;&nbsp; {{ trans('joinik.form.max_count_members.opts.no_limit') }}
                    </label>

                </div>

                <div class="input-group">
                <span class="input-group-addon">
                    <input name="is_limited_members" class="limit_members" type="radio" id="limit_members"
                           value="1">
                </span>
                    <input type="text" class="form-control" name="max_count_members" id="max_count_members"
                           placeholder="{{ trans('joinik.form.placeholder_max_count_members') }}"
                           value="{{ old('max_count_members', $joinik->max_count_members) }}"
                    >
                </div>
                <span class="glyphicon glyphicon-info"></span>


                <span class="help-block"></span>
            </div>

            <div class="form-group col-md-12">
                <label for="name" class="control-label">
                    <i class="fa fa-info-circle" data-toggle="popover"></i>
                    {{ trans('joinik.form.limit_age.label') }}
                </label>

                <div class="row">
                    <div class="col-md-2">
                        <select name="limit_age_from" id="" class="form-control">
                            <option value="0">{{ trans('joinik.form.limit_age.opts.from') }}</option>
                            @for ($i = 0; $i < 99; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-md-2">
                        <select name="limit_age_end" id="" class="form-control">
                            <option value="0">{{ trans('joinik.form.limit_age.opts.to') }}</option>
                            @for ($i = 0; $i < 99; $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>

                <span class="help-block"></span>
            </div>

        </div>
        <div class="form-group col-md-12">
            {{ trans('joinik.form.thumbnail_label') }}
            <input type="file" name="thumbnail" id="fileToUpload">
        </div>

        <div class="form-group col-md-12">
            <button type="submit" class="btn btn-default">{{ trans('joinik.form.submit') }}</button>
        </div>


        <div class="clearfix"></div>

    </form>
@endsection

@section('scripts')
    <script type="text/javascript">

        JoinikForm.init();
    </script>

@endsection