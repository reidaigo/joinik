<!-- JavaScript Libraries -->
{{--<script src="/lib/jquery/jquery.min.js"></script>--}}
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="/lib/php-mail-form/validate.js"></script>
<script src="/lib/prettyphoto/js/prettyphoto.js"></script>
<script src="/lib/isotope/isotope.min.js"></script>
<script src="/lib/hover/hoverdir.js"></script>
<script src="/lib/hover/hoverex.min.js"></script>

<!-- Template Main Javascript File -->
<script src="/js/main.js"></script>
{{--<script src="/js/app.js"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>