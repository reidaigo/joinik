<!-- Google Fonts -->


<!-- Bootstrap CSS File -->
<link href="/lib/bootstrap/css/bootstrap.css" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="/lib/prettyphoto/css/prettyphoto.css" rel="stylesheet">
<link href="/lib/hover/hoverex-all.css" rel="stylesheet">

<!-- Main Stylesheet File -->
{{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
<link href="/css/style.css" rel="stylesheet">

<!-- =======================================================
  Template Name: Solid
  Template URL: https://templatemag.com/solid-bootstrap-business-template/
  Author: TemplateMag.com
  License: https://templatemag.com/license/
======================================================= -->