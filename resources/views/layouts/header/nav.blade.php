<!-- Fixed navbar -->
<div class="container-fluid navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">{{ config('app.name', 'JOINIK') }}</a>
        </div>
        <div class="navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="/joinik/create">ДОБАВИТЬ ДЖОЙНИК</a></li>
                <li><a href="/feed">ЛЕНТА</a></li>
                @include('layouts.header.auth')
            </ul>
        </div>

        <!--/.nav-collapse -->
    </div>
</div>