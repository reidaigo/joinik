<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <title>@yield('title'){{ config('app.name', 'Joinik') }}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Favicon -->
    <link href="/favicon.ico" rel="icon">

@include('layouts.header.stylesheet')
</head>
<body>
@include('layouts.header.nav')

