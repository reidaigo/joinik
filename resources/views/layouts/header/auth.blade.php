<!-- Authentication Links -->

@guest
    <li><a href="{{ route('login') }}">ВОЙТИ</a></li>
    <li><a href="{{ route('register') }}">РЕГИСТРАЦИЯ</a></li>
@else
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
           aria-expanded="false" aria-haspopup="true">
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <ul class="dropdown-menu">
            <li class="dropdown-item">
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                    ВЫЙТИ
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
            <li><a href="single-project.html">SINGLE PROJECT</a></li>
            <li><a href="single-project.html">SINGLE PROJECT</a></li>
        </ul>
    </li>
@endguest