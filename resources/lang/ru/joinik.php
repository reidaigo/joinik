<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Joinik language files
    |--------------------------------------------------------------------------
    */

    'titles' => [
        'create' => 'Создать',
    ],

    'form' => [
        'name'                    => 'Название',
        'description'             => 'Описание',
        'additional_options'             => 'Дополнительные настройки',

        'place_str'             => [
            'label' => 'Адрес',
            'placeholder' => 'Улица/переулок/проспект, дом',
        ],

        'place_add_info'             => [
            'label' => 'Дополнительная информация по адресу',
            'placeholder' => 'подъезд 5 / квартира 10 / около магазина ',
        ],

        'type_available'          => [
            'label' => 'Видна',
            'opts'  => [
                'public'   => 'всем',
                'friendly' => 'друзьям',
                'private'  => 'по ссылке',
            ],
        ],
        'date_start'              => 'Начало',
        'date_end'                => 'Конец',
        'protected'               => 'требуется потверждение на участие',
        'limit_age'               => [
            'label' => 'Возраст',
            'opts'  => [
                'from' => 'от',
                'to'   => 'до',
            ],
        ],
        'max_count_members'       => [
            'label' => 'Лимит количества участников',
            'opts'  => [
                'no_limit' => 'без лимита',
                'limit'    => 'с лимитом в',
            ],
        ],

        'submit' => 'Создать',

        'placeholder_name'              => 'Название',
        'placeholder_description'       => 'Описание',
        'placeholder_date_start'        => 'Начало',
        'placeholder_date_end'          => 'Конец',
        'placeholder_max_count_members' => 'Введите макс кол-во участников',

        'thumbnail_label' => 'По умолчанию ваш аватар будет загружен как аватар встречи. При необходимости загрузите другой аватар встречи.',
    ],

];
