<?php


namespace App\Repositories;


use App\Joinik;

class JoinikRepository
{

    private $joinik;

    private $user;

    public function __construct(Joinik $joinik)
    {
        $this->joinik = $joinik;
        $this->user = \Auth::user();
    }

    public function store($data) {
        Joinik::create($data);
    }

}