<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JoinikPlace extends Model
{
    protected $fillable = [
        'street',
        'additional_info',
        'coords_x',
        'coords_y'
    ];
}
