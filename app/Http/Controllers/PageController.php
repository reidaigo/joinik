<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Joinik;

class PageController extends Controller
{
    /**
     * Display main page.
     *
     * @return \Illuminate\Http\Response
     */
    public function main()
    {
        $joiniks = Joinik::all();
        return view('main', compact('joiniks'));
    }
}
