<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joinik extends Model
{
    protected $fillable = [
//        'author_id',
        'title',
        'description',
        'type_available',
        'protected',
        'is_limited_members',
        'max_count_members',
        'limit_age_from',
        'limit_age_to',
//        'thumbnail',
//        'deleted',
        'date_start',
        'date_end'
    ];

    protected $guarded = ['id'];

    public function places()
    {
        return $this->hasMany(JoinikPlace::class);
    }

}
