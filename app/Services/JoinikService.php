<?php

namespace App\Services;


use App\Http\Requests\StoreJoinik;
use App\Joinik;
use App\User;
use Illuminate\Support\Facades\Auth;

class JoinikService extends BaseService
{

    public static function get($id)
    {
        return Joinik::find($id);
    }

    public static function store(StoreJoinik $request)
    {
        if ($user = Auth::user()) {

            if ($request->has('thumbnail')) {
                $thumbnail = $request->file('thumbnail')->store('joinik/thumbnails');
            } else {
                $thumbnail = $user->avatar;
            }

            $request->merge([
                'thumbnail' => $thumbnail
            ]);

            $joinik = $user->joiniks()->create($request->all());
            $joinik->places()->create($request->all());
            dd($joinik);
        }
        else {
            die('dsdsds');
        }

    }
}